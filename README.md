# 212 Computer Science

* Project 1 - Introduction to Java
* Project 2 - Array vs ArrayList Data Structure basics
* Project 3 - Java Class Hierarchy
* Project 4 - Java UI Interface - Paint Application
* Project 5 - Sorting Algorithms: Selection Sort and Merge Sort
* Project 6 - Building Data Structure - Occurence Set
* Project 7 - Concurrent Programming via Multiple Threads and Synchronization
* Project 8 - Network Programming - Server Client Communication
